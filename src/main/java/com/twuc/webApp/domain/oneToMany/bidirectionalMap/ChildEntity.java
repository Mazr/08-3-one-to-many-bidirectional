package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import javax.persistence.*;

// TODO
//
// 请使用双向映射定义 ParentEntity 和 ChildEntity 的 one-to-many 关系。其中 ChildEntity
// 的数据表应当为如下的结构。
//
// child_entity
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <-start-
@Entity
public class ChildEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 20,nullable = false)
    private String name;

    public void setParent(ParentEntity parent) {
        this.parent = parent;
    }

    public ParentEntity getParent() {
        return parent;
    }

    @ManyToOne
    @JoinColumn(name = "parent_entity_id",referencedColumnName = "id")
    private ParentEntity parent;

    public ChildEntity(String name) {
        this.name = name;
    }

    public ChildEntity() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
// --end->